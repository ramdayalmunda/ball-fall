import template from "./ball.template.js";

const { inject, onUnmounted } = Vue;
export default {
    template,
    setup(){
        let setup = inject("setup")
        let gameSettings = inject("gameSettings")

        function ballEvents(){

            setup.ball.position.left += setup.ball.movement.x
            setup.ball.position.top += setup.ball.movement.y

            // game over condition
            if (setup.ball.position.top > setup.canvas.dimension.height){
                gameSettings.gameOver = true
                return
            }
            
            // walls
            if ( setup.ball.movement.x<0 && setup.ball.position.left<0 ){
                setup.ball.movement.x = -setup.ball.movement.x;
            }
            if ( setup.ball.movement.y<0 && setup.ball.position.top<0 ){
                setup.ball.movement.y = -setup.ball.movement.y
            }
            if ( setup.ball.movement.x>0 && setup.ball.position.left+setup.ball.dimension.width>setup.canvas.dimension.width ){
                setup.ball.movement.x = -setup.ball.movement.x
            }

            if (
                setup.ball.movement.y>0 &&
                setup.ball.position.top + setup.ball.dimension.height >= setup.bar.position.top &&
                setup.ball.position.top + setup.ball.dimension.height <= setup.bar.position.top + ( setup.bar.dimension.height/4 ) &&
                setup.ball.position.left + (setup.ball.dimension.width*1.1)>setup.bar.position.left &&
                setup.ball.position.left - (setup.ball.dimension.width*0.1) < setup.bar.position.left+setup.bar.dimension.width
            ){
                setup.ball.movement.y = -setup.ball.movement.y

                let ballX = setup.ball.position.left + setup.ball.dimension.width/2 - setup.bar.position.left;
                let direction = (ballX>setup.bar.dimension.width/2)?1:-1
                let percent = (ballX/setup.bar.dimension.width)*100
                let degree = percent*9/5
                let radian = degree*Math.PI/180
                setup.ball.movement.y = -Math.abs(setup.ball.movement.speed*Math.sin(radian))
                setup.ball.movement.x = Math.abs(setup.ball.movement.speed*Math.cos(radian)) *direction

                clearTimeout(setup.bar.bounceTimeout)
                setup.bar.bounced = true
                setup.bar.bounceTimeout = setTimeout( ()=>{ setup.bar.bounced = false }, 100 )
                gameSettings.score += 10 + setup.ball.movement.speed
                gameSettings.bounces ++
                return

            }


        }

        function increaseSpeed(){
            stopBall()
            setup.ball.movement.speed += 1
            setup.ball.intervalHolder = setInterval( ballEvents, gameSettings.timePerFrame )
        }

        function moveBall(){
            stopBall()
            setup.ball.intervalHolder = setInterval( ballEvents, gameSettings.timePerFrame )
        }
        function stopBall(){
            if (setup.ball.intervalHolder) clearInterval(setup.ball.intervalHolder)
            setup.ball.intervalHolder = null
        }

        onUnmounted( ()=>{
            clearInterval( setup.ball.intervalHolder );
            setup.ball.intervalHolder = null
        } )

        return {
            setup,
            moveBall,
            stopBall,
            increaseSpeed,
        }
    },
}