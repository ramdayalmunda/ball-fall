export default /*html*/`

<span id="game-ball" class="game-ball"
    :style="
        'height:'+setup.ball.dimension.height+'px;'+
        'width:'+setup.ball.dimension.width+'px;' +
        'left:'+setup.ball.position.left+'px;' +
        'top:'+setup.ball.position.top+'px;'
    "
></span>

`