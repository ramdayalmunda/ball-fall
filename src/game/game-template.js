export default /*html*/`
<div id="game-canvas" class="game-canvas"
    :style="
        'height:'+setup.canvas.dimension.height+'px;'+
        'width:'+setup.canvas.dimension.width+'px;'+
        'left:'+setup.canvas.position.left+'px;'+
        'top:'+setup.canvas.position.top+'px;'
    ">
    <template v-for="(obstacle, o) in obstaclesList" >
        <Obstacle ref="obstacle.ref" :setup="obstacle.setup" ></Obstacle>
    </template>
    <Bar ref="barRef"></Bar>
    <Ball ref="ballRef" ></Ball>
</div>
<label>Score: {{gameSettings.score}}</label><br />
<label>Bounces: {{gameSettings.bounces}}</label><br />
<button type="button" v-show="!gameSettings?.started" @click="startGame">Start Game</button><br/>
<button type="button" v-show="gameSettings?.started && !gameSettings?.gameOver && !gameSettings?.paused" @click="pauseGame">Pause</button><br/>
<button type="button" v-show="gameSettings?.started && !gameSettings?.gameOver && gameSettings?.paused" @click="startGame">Resume</button><br/>
<label v-show="gameSettings?.gameOver">Game Over</label>

`