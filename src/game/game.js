import template from "./game-template.js";
import Ball from "./ball/ball.js";
import Bar from "./bar/bar.js";
import Obstacle from "./obstacle/obstacle.js";
import { getDefaultSetup, getObstacleSetup } from "../setup.js";
const { inject, onMounted, ref, onUnmounted, watch } = Vue;
export default {
    components: { Ball, Bar, Obstacle },
    template,
    setup() {
        const setup = inject("setup")
        const gameSettings = inject("gameSettings")

        let ballRef = ref(null);
        let barRef = ref(null);
        let obstaclesList = ref([{ ref: null, setup: getObstacleSetup() }])

        const defaultSettings = getDefaultSetup()

        watch( gameSettings, (settings)=>{
            if (settings.gameOver || settings.paused){
                pauseGame()
                return
            }

            if (settings.bounces%2==0){
                ballRef.value.increaseSpeed()
            }

        } )

        function setDefaults (){
            for ( const key in defaultSettings ){

                if (defaultSettings[key].elementName){
                    defaultSettings[key].element = document.getElementById(defaultSettings[key].elementName)
                }
                setup[key] = defaultSettings[key]
            }
        }

        function startGame(){
            gameSettings.started = true
            gameSettings.paused = false;
            barRef.value.listenCursor()
            ballRef.value.moveBall()   
        }
        function pauseGame(){
            gameSettings.started = true;
            gameSettings.paused = true;
            barRef.value.unListenCursor()
            ballRef.value.stopBall()   
        }

        function keyEvents(e){
            if (e.key == ' ' && (!gameSettings.started || gameSettings.paused ) ){
                startGame()
            }
            else if ( (e.key=="Escape" || e.key=="Backspace") && !gameSettings.paused && gameSettings.started  ){
                pauseGame()
            }
        }

        onMounted( ()=>{
            setDefaults();
            addEventListener( 'keydown', keyEvents)
        } )
        onUnmounted( ()=>{
            removeEventListener( 'keydown', keyEvents)
        } )
        return {
            setup,
            gameSettings,
            ballRef,
            barRef,
            obstaclesList,
            
            startGame,
            pauseGame,
        }
    },
}