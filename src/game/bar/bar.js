import template from "./bar.template.js";

const  { inject, onUnmounted } = Vue;
export default {
    template,
    setup(){
        let setup = inject("setup");
        let gameSettings = inject("gameSettings")

        function handleCursor(e){
            if ( !gameSettings?.started || gameSettings?.paused || gameSettings?.gameOver ) return
            let left = e.clientX - setup.canvas.position.left - (setup.bar.dimension.width/2)
            let leftLimit = -(setup.bar.dimension.width/2);
            let rightLimit = (setup.canvas.dimension.width - (setup.bar.dimension.width/2))
            left = left<leftLimit?leftLimit:left
            left = left>rightLimit?rightLimit:left
            setup.bar.position.left = left
        }

        function listenCursor(){
            document.addEventListener('mousemove', handleCursor)
        }
        function unListenCursor(){
            document.removeEventListener('mousemove', handleCursor)
        }

        onUnmounted( ()=>{
            document.removeEventListener('mousemove', handleCursor)
        } )

        return {
            setup,
            handleCursor,
            listenCursor,
            unListenCursor,
        }
    }
}