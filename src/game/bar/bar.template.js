export default /*html*/`

<span id="game-bar" class="game-bar"

    :style="
        'height:'+setup.bar.dimension.height+'px;'+
        'width:'+setup.bar.dimension.width+'px;' +
        'left:'+setup.bar.position.left+'px;' +
        'top:'+setup.bar.position.top+'px;' +
        'border-radius:'+setup.bar.dimension.borderRadius+'px;' +
        (setup.bar.bounced?'border-top: solid 5px #dddddd;':'')
    "
></span>

`