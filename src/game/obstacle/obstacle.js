const { onMounted } = Vue;
export default {
    props: { setup: Object },
    setup( props ){
        let id = `${new Date().getTime()}`

        onMounted(()=>{
            console.log('asdasd ', props.setup)
        })
        return {
            id
        }
    },
    template: /*html*/`
    <span :id="id" name="game-obstacle" class="game-obstacle"
        :style="
            'width:'+setup.dimension.width+'px;' +
            'height:'+setup.dimension.height+'px;' +
            'left:'+setup.position.left+'px;' +
            'top:'+setup.position.top+'px;' +
            'border-radius:'+setup.dimension.borderRadius+ 'px;'
        "
    ></span>
    `   
}