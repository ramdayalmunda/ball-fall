import GameComponent from "./game/game.js"
import { gameSettings, setup } from "./setup.js"

const VueApp = Vue.createApp({
    components: {
        GameComponent,
    },
    template: `
    <GameComponent></GameComponent>
    `
})


VueApp.provide('setup', setup)
VueApp.provide('gameSettings', gameSettings)
VueApp.mount("#vue-div")