export function getDefaultSetup() {
    const setupSetting = {
        canvas: {
            element: null,
            elementName: "game-canvas",
            dimension: { height: 600, width: 400 },
            position: { left: 100, top: 50 },
        },
        ball: {
            element: null,
            elementName: "game-ball",
            dimension: { height: 25, width: 25 },
            position: { left: 0, top: 0 },
            movement: { x: 3, y: 5, degree: 30, speed: 5 },
            intervalHolder: null,
        },
        bar: {
            element: null,
            elementName: "game-bar",
            dimension: { height: 30, width: 150, borderRadius: 10 },
            position: { left: 0, top: 550 },
            bounced: false,
            bounceTimeout: null,
        }
    }
    setupSetting.canvas.element = document.getElementById('game-canvas')
    return setupSetting
}

export function getObstacleSetup(){
    return {
        dimension: {height: 30, width: 40, borderRadius: 5 },
        position: { left: 100, top: 200 }
    }
}

export function getDefaultSettings (){
    return {
        started: false,
        paused: false,
        gameOver: false,
        timePerFrame:10,
        score: 0,
        bounces: 0,
    }
}

export const gameSettings = Vue.reactive(getDefaultSettings())
export const setup =  Vue.reactive(getDefaultSetup())